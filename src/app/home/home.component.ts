import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  Traders: any = [];
  @Input() traderDetails = { id: 0, ticker: '', price: 0, quantity: 0, buyOrSell: ''}
  
  verbose: boolean;

  constructor( 
    public restApi: RestApiService,
    public router: Router,
    ) {this.verbose = true; }

    

  ngOnInit(): void {
    this.loadTraders()
  }

    //Reloads the database
  loadTraders() {
    return this.restApi.getTraders().subscribe((data: {}) => {
      this.Traders = data;
    })
  }

  addTraders() {
    this.restApi.createTrader(this.traderDetails).subscribe((data: {}) => {
      this.router.navigate(['/trader-list'])
      this.loadTraders();
    })
  }

  // Label for Showing/Hiding Items

  label() {
    return (this.verbose) ? 'Show' : 'Hide';
  }  
  
  onVerboseToggle(event: any) {
    this.verbose = !this.verbose;
  }

  // End of Label 
}