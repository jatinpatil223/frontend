import { Component, OnInit, Input, ViewChild, ElementRef} from '@angular/core';
import { HttpClient } from "@angular/common/http";
//import { URL } from 'url';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})

export class WatchlistComponent {

  Ticker: string = '';

  public data:any = []; /// This accepts a json object

  liveStockURL: string;

  constructor(public http: HttpClient) {
    this.liveStockURL = ("https://qz4sxjl623.execute-api.us-east-1.amazonaws.com/default/tradeAdvisor?ticker=");
   
 }

 getData(){
  this.http.get(this.liveStockURL.concat(this.Ticker)).subscribe((res)=>{
    this.data = res
   //this.liveStockURL.concat(this.Ticker);
    console.log(this.data)
    })
  }
}
