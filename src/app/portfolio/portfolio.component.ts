import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  constructor(public restApi: RestApiService) { }

  myPortfolio: any = []; //For portfolio
  
  ngOnInit(): void {
  }

  returnPortfolio(){
    return  this.restApi.getStocksTickerwise().subscribe((data: {}) => {
    this.myPortfolio = data;
    console.log(this.myPortfolio);
  })}
}
