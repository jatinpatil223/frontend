import { Component, OnInit, Input } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";

import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {

  Traders: any = [];
  SearchBool : Boolean;

  @Input() traderDetails = { id: 0, ticker: '', price: 0, quantity: 0, buyOrSell: ''}

  constructor( public restApi: RestApiService) { this.SearchBool = true;}

  searchText = '';
  characters = [
    'AMZN',
    'SAIL',
    'GAIL',
    'INFY',
    'TSLA',
    'PNB',
    'AXIS.NS',
    'AAPL',
  ]
  //Dynamically create this array based on current stocks we hold (Portfolio tickers)

  ngOnInit(): void {
  }

  myStockSearch() {
    //accept input and perform search in the existing trading history
    return this.restApi.getTraderByName(this.searchText).subscribe((data: {}) => {
      this.Traders = data;
      this.SearchBool = !this.SearchBool; 
    }
    )
    //https://dev.to/idrisrampurawala/creating-a-search-filter-in-angular-562d
    //Reference code

    //Removes search results if submit has been pressed 
  }
  DidYouSearch(event:any){
    this.SearchBool = false;
  }
}
