import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Trader } from '../shared/trader';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  apiURL = 'http://trading-app-trading-app.punedevopsb3.conygre.com:80/api/stocks/';
  apiURL2 = 'http://trading-app-trading-app.punedevopsb3.conygre.com:80/api/stocks/';

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  // HttpClient API get() method 
  getTraders(): Observable<Trader> {
    return this.http.get<Trader>(this.apiURL2)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method 
  getTrader(id:any): Observable<Trader> {
    return this.http.get<Trader>(this.apiURL2 + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  
//New Function added for getting stock history by ticker

  getTraderByName(ticker:any):Observable<Trader> {
    return this.http.get<Trader>(this.apiURL2 + ticker)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

// DONE --------

// New Function 2 added for PORTFOLIO thing ---
getStocksTickerwise():Observable<Trader> {
  return this.http.get<Trader>(this.apiURL2 +"portfolio")
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
}

//----Done----

  // HttpClient API post() method 
  createTrader(trader:Trader): Observable<Trader> {
    // This line is creating problems :/
    //Now using api url 2 which leads to api/stocks
    return this.http.post<Trader>(this.apiURL2 + 'post', JSON.stringify(trader), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } 

  // HttpClient API put() method
  updateTrader(id:number, trader:Trader): Observable<Trader> {
    //Now using api url 2 which leads to api/stocks
    return this.http.put<Trader>(this.apiURL2, JSON.stringify(trader), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method 
  deleteTrader(id:number){
    return this.http.delete<Trader>(this.apiURL + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Error handling 
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
  
}
