export class Trader {
    constructor(){
        this.id = 0;
		this.ticker = "";
		this.price = 0;
        this.buyOrSell = "";
        this.quantity= 0;
    }

    id: number;
	ticker: string;
	price: number;
    buyOrSell: string;
    quantity: number;
}
